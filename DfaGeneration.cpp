/*Program to generate dfa for accepting numbers divisible by another number*/

#include <iostream>
#include <map>
#include <iterator>
#include <bitset>
#include <string>
using namespace std;

map <int , int > dfaonZ;
map <int , int > dfaonO;

//Definition for getting the edges of dfa
//num - input number given by the user

void dfa(int num) {	
	int nextnode;
	for(int i = 1;i<num;i++) {
		nextnode = (2*i)%num;		//for 2x
		dfaonZ.insert(pair <int ,int > (i,nextnode));
		nextnode = (2*i+1)%num;		//for 2x+1
		dfaonO.insert(pair <int ,int > (i,nextnode));
	}
}
void printdfa(int num) {			//prints dfa to dotty file
	FILE *pipe,*fp;
	map <int , int >::iterator it;
	fp = fopen("Dfa.dot" , "w");
 	fprintf(fp,"digraph dfa{ \n");
  	for(int i=0;i<num;i++) 
    		fprintf(fp,"%d [label=\"%d\",color=black];\n",i,i);

	for(it = dfaonZ.begin();it!=dfaonZ.end();++it)
			fprintf(fp,"%d -> %d [label = \" %d \",color = black];\n",it->first,it->second,0);
	for(it = dfaonO.begin();it!=dfaonO.end();++it)
			fprintf(fp,"%d -> %d [label = \" %d \",color = black];\n",it->first,it->second,1);
	
	fprintf(fp,"}\n");
  	fclose(fp);

  	pipe=popen("dot -Tps Dfa.dot -o Dfa.ps","w");
  	pclose(pipe);
  	pipe=popen("evince Dfa.ps","r");
  	pclose(pipe);
}
//Shows the working of dfa
//b - input binary number for which the working must be shown
//num - number entered by the user
//len - length of the input binary number
void Working(bitset<32> b,int num,int len) {
	FILE *pipe,*fp;
	map <int , int >::iterator it;
	int state = 0,state1;
	fp = fopen("Dfa2.dot" , "w");
	cout<<state<<endl;
	for(int j=len-1;j>=0;j--) {
		state1 = state;					//state in which the dfa is currently in with j input symbols
	 	fprintf(fp,"digraph dfa%d{ \n",(len-j+1));
	  	for(int i=0;i<num;i++) 
	    		fprintf(fp,"%d [label=\"%d\",color=black];\n",i,i);
	    
	    for(it = dfaonZ.begin();it!=dfaonZ.end();++it){
	    	if((it->first == state1)&&(b[j]==0)){
	    		fprintf(fp,"%d -> %d [label = \" %d \",color = black,penwidth=2];\n",it->first,it->second,0);		//edge marked bold
	    		state = it->second;

	    	}
	    	else
	    	   	fprintf(fp,"%d -> %d [label = \" %d \",color = black];\n",it->first,it->second,0);
	    }
			
		for(it = dfaonO.begin();it!=dfaonO.end();++it){
			if((it->first == state1)&&(b[j]==1)){
				fprintf(fp,"%d -> %d [label = \" %d \",color = black,penwidth=2];\n",it->first,it->second,1);		//edge marked bold
	    		state = it->second;	
			}	
			else
				fprintf(fp,"%d -> %d [label = \" %d \",color = black];\n",it->first,it->second,1);
		}
		fprintf(fp,"}\n");
  	
  	}
  	fclose(fp);
  	pipe=popen("dot -Tps Dfa2.dot -o Dfa2.ps","w");
  	pclose(pipe);
  	pipe=popen("evince Dfa2.ps","r");
  	pclose(pipe);

}
string removeZero(string str) { //removes trailing 0 in the string
	int i=0;
	while(str[i] == '0')
		i++;
	str.erase(0,i);
	return str;
}
int main() {

	int num;
	int ch;
	int len;
	string str;
	cout<<"Enter the number : \n";  //input number
	cin>>num;
	
	bitset<32> b;
	dfaonZ.insert(pair <int ,int > (0,0)); 		//insert 0 0 edge 
	dfaonO.insert(pair <int ,int > (0,1));		//insert 0 1 edge
	dfa(num);							//get the dfa
	
	while(1) {
		cout<<"1. Print dfa\n2. Show the working\n3. Exit\nEnter your choice :";
		cin>>ch;
		switch(ch) {
			case 1: printdfa(num);				//prints dfa
					break;
			case 2: cout<<"Enter a binary number : ";
					cin>>b;	
					str = b.to_string();		//binary number converted to string
					str = removeZero(str);		//trailing 0s are removed
					len = str.length();			//length of the binary number that needs to be checked is noted
					Working(b,num,len);			//shows the working
					break;
			case 3: return 0;
			default: cout<<"Invalid choice\n";
		}
	}
}

